package com.example.pavel.toothpickdemo.app_scope;

import android.app.Application;
import android.support.annotation.Nullable;
import android.util.Log;


import javax.inject.Inject;
import javax.inject.Singleton;

import toothpick.Lazy;

import static com.example.pavel.toothpickdemo.App.TAG;

/**
 * Created by Pavel on 11/14/2017.
 */
@Singleton
public class AppScopedClass1 {

    @Inject
    public AppScopedClass1(Lazy<AppScopedClass3> appContext3,
                           Application app,
                           Foo foo,
                           @Nullable UnderDevelopment underDevelopment) {
        Log.d(TAG, "AppScopedClass1() called with: appContext3 = [" + appContext3 + "], app = [" + app + "], foo = [" + foo + "], underDevelopment = [" + underDevelopment + "]");
        appContext3.get();
    }
}
