package com.example.pavel.toothpickdemo.activity_scope;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

import com.example.pavel.toothpickdemo.app_scope.AppScopedClass1;

import javax.inject.Inject;

import static com.example.pavel.toothpickdemo.App.TAG;

/**
 * Created by Pavel on 11/14/2017.
 */
@MainActivityScope
public class ActivityScopedClass1 {

    @Inject
    public ActivityScopedClass1(AppScopedClass1 appScopedClass1, ActivityScopedClass2 activityScopedClass2,
                                Activity activity, Application app) {
        // activityScopedClass2 and activity are different for each activity instance,
        // but appScopedClass1 and app are the same.
        Log.d(TAG, "ActivityScopedClass1() called with: appScopedClass1 = [" + appScopedClass1 + "], activityScopedClass2 = [" + activityScopedClass2 + "], activity = [" + activity + "], app = [" + app + "]");
    }
}
