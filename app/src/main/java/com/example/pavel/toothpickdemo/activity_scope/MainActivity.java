package com.example.pavel.toothpickdemo.activity_scope;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.pavel.toothpickdemo.App;
import com.example.pavel.toothpickdemo.R;
import com.example.pavel.toothpickdemo.vh_scope.ViewHolder;

import javax.inject.Inject;

import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

public class MainActivity extends AppCompatActivity {


    // In "scope holders" such as an activity we inject those classes that need to start working
    // immediately when scope is created.
    // No actual application logic code needs to be in the activity itself, rather it should be
    // in corresponding classes, so that SRP is being followed and testability ensured.
    @Inject ActivityScopedClass1 activityScopedClass1;

    private Scope scope;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // here we specify the parent scope (app) and the new one (activity). Very simple.
        scope = Toothpick.openScopes(getApplication(), this);

        scope.bindScopeAnnotation(MainActivityScope.class);
        scope.installModules(new Module() {{
            bind(Activity.class).toInstance(MainActivity.this);
        }});
        Toothpick.inject(this, scope);
        Log.d(App.TAG, "MainActivity onCreate: injected " + activityScopedClass1);

        ViewHolder vh1 = new ViewHolder(this);
        ViewHolder vh2 = new ViewHolder(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toothpick.closeScope(this);
    }


}
