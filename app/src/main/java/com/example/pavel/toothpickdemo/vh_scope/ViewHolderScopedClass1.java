package com.example.pavel.toothpickdemo.vh_scope;

import android.util.Log;

import com.example.pavel.toothpickdemo.activity_scope.ActivityScopedClass1;

import javax.inject.Inject;

import static com.example.pavel.toothpickdemo.App.TAG;

/**
 * Created by Pavel on 11/14/2017.
 */
@ViewHolderScope
public class ViewHolderScopedClass1 {

    @Inject
    public ViewHolderScopedClass1(ActivityScopedClass1 activityScopedClass1,
                                  ViewHolder viewHolder) {
        Log.d(TAG, "ViewHolderScopedClass1() called with: activityScopedClass1 = [" + activityScopedClass1 + "], viewHolder = [" + viewHolder + "]");
    }
}
