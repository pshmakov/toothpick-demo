package com.example.pavel.toothpickdemo.app_scope;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Pavel on 11/14/2017.
 */
@Singleton
public class FooImpl2 implements Foo {
    @Inject
    public FooImpl2(AppScopedClass3 appScopedClass3) {}
}
