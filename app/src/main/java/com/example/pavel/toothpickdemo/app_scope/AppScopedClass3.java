package com.example.pavel.toothpickdemo.app_scope;

import android.content.res.Resources;
import android.util.Log;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.example.pavel.toothpickdemo.App.TAG;

/**
 * Created by Pavel on 11/14/2017.
 */
@Singleton
public class AppScopedClass3 {

    @Inject
    public AppScopedClass3(AppScopedClass2 appScopedClass2, Resources resources) {
        Log.d(TAG, "AppScopedClass3() called with: appScopedClass2 = [" + appScopedClass2 + "]");
    }
}
