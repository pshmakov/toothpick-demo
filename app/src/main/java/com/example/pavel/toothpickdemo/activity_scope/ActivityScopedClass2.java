package com.example.pavel.toothpickdemo.activity_scope;

import android.util.Log;

import javax.inject.Inject;

import static com.example.pavel.toothpickdemo.App.TAG;

/**
 * Created by Pavel on 11/14/2017.
 */
@MainActivityScope
public class ActivityScopedClass2 {

    @Inject
    public ActivityScopedClass2() {
        Log.d(TAG, "ActivityScopedClass2() called");
    }
}
