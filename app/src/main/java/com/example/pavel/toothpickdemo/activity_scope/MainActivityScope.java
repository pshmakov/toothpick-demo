package com.example.pavel.toothpickdemo.activity_scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Pavel on 11/14/2017.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface MainActivityScope {}
