package com.example.pavel.toothpickdemo.vh_scope;

import android.util.Log;

import com.example.pavel.toothpickdemo.activity_scope.MainActivity;

import javax.inject.Inject;

import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

import static com.example.pavel.toothpickdemo.App.TAG;

/**
 * Created by Pavel on 12/1/2017.
 */

public class ViewHolder {
    private final Scope scope;

    @Inject
    ViewHolderScopedClass2 viewHolderScopedClass2;

    public ViewHolder(MainActivity activity) {
        scope = Toothpick.openScopes(activity, this);
        scope.bindScopeAnnotation(ViewHolderScope.class);
        scope.installModules(new Module() {{
            bind(ViewHolder.class).toInstance(ViewHolder.this);
        }});
        Toothpick.inject(this, scope);
    }
}
