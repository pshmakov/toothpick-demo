package com.example.pavel.toothpickdemo.app_scope;

import javax.inject.Singleton;

/**
 * Created by Pavel on 11/14/2017.
 */
@Singleton
public class AppScopedClass2 {
    public AppScopedClass2() {}
}
