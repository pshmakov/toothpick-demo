package com.example.pavel.toothpickdemo.vh_scope;

import android.util.Log;

import javax.inject.Inject;

import static com.example.pavel.toothpickdemo.App.TAG;

/**
 * Created by Pavel on 11/14/2017.
 */
@ViewHolderScope
public class ViewHolderScopedClass2 {
    @Inject
    public ViewHolderScopedClass2(ViewHolderScopedClass1 viewHolderScopedClass1) {
        Log.d(TAG, "ViewHolderScopedClass2() called with: viewHolderScopedClass1 = [" + viewHolderScopedClass1 + "]");
    }
}
