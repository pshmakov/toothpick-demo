package com.example.pavel.toothpickdemo.util;

import javax.inject.Provider;

/**
 * Created by Pavel on 11/14/2017.
 */

public class NullProvider implements Provider {
    public static final NullProvider INSTANCE = new NullProvider();

    private NullProvider() {}

    @Override
    public Object get() {
        return null;
    }
}
