package com.example.pavel.toothpickdemo.vh_scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Pavel on 12/1/2017.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ViewHolderScope {
}
