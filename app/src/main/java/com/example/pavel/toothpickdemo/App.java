package com.example.pavel.toothpickdemo;

import android.app.Application;
import android.content.res.Resources;
import android.util.Log;

import com.example.pavel.toothpickdemo.app_scope.AppScopedClass1;
import com.example.pavel.toothpickdemo.app_scope.Foo;
import com.example.pavel.toothpickdemo.app_scope.FooImpl1;
import com.example.pavel.toothpickdemo.app_scope.FooImpl2;
import com.example.pavel.toothpickdemo.app_scope.UnderDevelopment;
import com.example.pavel.toothpickdemo.util.NullProvider;

import javax.inject.Inject;

import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;
import toothpick.configuration.Configuration;
import toothpick.registries.FactoryRegistryLocator;
import toothpick.registries.MemberInjectorRegistryLocator;

/**
 * Created by Pavel on 11/14/2017.
 */

public class App extends Application {

    public static String TAG = "ToothpickTest";

    @Inject AppScopedClass1 appScopedClass1;
    private Scope scope;

    @Override
    public void onCreate() {
        super.onCreate();
        configureToothpick();
        scope = Toothpick.openScope(this);
        scope.installModules(new AppModule());
        Toothpick.inject(this, scope);
    }

    private void configureToothpick() {
        Toothpick.setConfiguration(Configuration.forProduction().disableReflection());
        // In no-reflection mode Toothpick generates "Registries" which mappings between application classnames and generated factories
        FactoryRegistryLocator.setRootRegistry(new com.example.pavel.toothpickregistries.FactoryRegistry());
        MemberInjectorRegistryLocator.setRootRegistry(new com.example.pavel.toothpickregistries.MemberInjectorRegistry());
    }

    // Modules are not required generally, but they are useful if you need alternative or custom implementations to inject
    private class AppModule extends Module {
        public AppModule() {
            // It might be convenient to inject instances of Application or Resources or something else somewhere
            bind(Application.class).toInstance(App.this);
            bind(Resources.class).toInstance(getResources());

            boolean someFeatureEnabled = true;
            if (someFeatureEnabled) {
                // Either FooImpl1 or FooImpl2 are injected under the Foo interface
                bind(Foo.class).to(FooImpl1.class);
            } else {
                bind(Foo.class).to(FooImpl2.class);
            }

            boolean releaseBuild = true;
            if (releaseBuild) {
                // This is an alternative to Optional<>, which requires Java8.
                // in "release build" null will be injected instead of UnderDevelopment instance
                bind(UnderDevelopment.class).toProviderInstance(NullProvider.INSTANCE);
            }
        }
    }
}
